GLOBAL find_word

EXTERN string_equals


section .text
; rdi - pointer to null-terminated string
; rsi - pointer to the start of dictionary (label)
; returns pointer to dict entry if success, otherwise 0
find_word:
	.loop:
		push rdi				; saving values before function call
		push rsi
		add	 rsi, 8				; moving pointer to key of dict element
		call string_equals		; checking if there is key we are looking for
		pop  rsi				; geting values back
		pop  rdi
		cmp	 rax, 1				; check if strings are equal
		je	 .found				; if there equal -> .found
		cmp  qword[rsi], 0		; check if it was the last dict element
		je   .nf 				; if it was the last -> not found -> .nf
		mov  rsi, [rsi]			; getting next dict element by label
		jmp  .loop				; repeat :)
	.found:
		mov  rax, rsi			; we found correct element, so return pointer to it
		ret
	.nf:
		mov  rax, 0				; return 0, because there are no such key in dict
		ret