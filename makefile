%.o: %.asm
	nasm -felf64 -o $@ $^

main: main.o lib.o dict.o
	ld -o $@ $^
	rm *.o