%include "main.inc"

%define BUF_SIZE 256

GLOBAL _start

section .bss
buffer: RESB BUF_SIZE

section .rodata
not_found_mes:  db "В словаре не найдено значений по введенному ключу", `\n`, 0
too_long_input: db "Вы ввели строку длиной более 255 символов", `n`, 0

%include "words.inc"

section .text
_start:
	mov  rdi, buffer	; preparing args for read_line, rdi - buffer pointer
	mov  rsi, BUF_SIZE	; rsi - buffer length
	call read_line		; returns buffer pointer - rax, length - rdx
	test rax, rax		; check if read success
	jz   .too_long_err  ; if not success - line was too long
	mov  rsi, label		; preparing args for find_word, rsi - label to dict's first elem
	mov  rdi, buffer	; rdi - pointer to key, that user inputed
	call find_word
	test rax, rax		; if rax != 0 -> element found
	jnz  .success
	mov  rdi, not_found_mes		; element not found -> print error about it
	call print_err
	jmp  exit
	.success:
		mov  rdi, rax			; getting pointer to dict's element's value
		call get_value
		mov  rdi, rax			; printing it
		call print_string
		call print_newline		
		jmp  exit
	.too_long_err:
		mov  rdi, too_long_input	; printing error about too long input from user
		call print_err
		jmp  exit


; returns pointer to value of dict element
; 1st arg - pointer to dict element (rdi)
get_value:
	add  rdi, 8				; put pointer to elem's key
	push rdi
	call string_length		; getting key's length
	pop  rdi
	inc  rdi
	add  rax, rdi 			; put pointer to elem's value
	ret