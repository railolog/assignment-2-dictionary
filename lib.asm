%include "lib.inc"

section .text

%define EXIT_SYSCALL 60

; Принимает код возврата и завершает текущий процесс
exit: 
    ;mov rbx, rdi    ; exit code
    xor rdi, rdi
    mov rax, EXIT_SYSCALL     ; 'exit' syscall code
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .next:
        cmp byte[rdi + rax], 0  ; comparing with null-terminator
        je .exit                ; if equals exit
        inc rax                 ; inc returning value
        jmp .next               ; repeat
    .exit:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi                ; saving reg value
    call string_length      ; counting length
    pop rsi                 ; getting pointer to string
    mov rdx, rax            ; getting string length
    mov rax, 1              ; 'write' syscall code
    mov rdi, 1              ; stdout descriptor
    syscall
    ret


; prints string to stderr
; 1st arg - pointer to null-terminated string
print_err:
    push rdi
    call string_length
    pop  rsi
    mov  rdx, rax
    mov  rax, 1
    mov  rdi, 2
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    pop rdi
    mov rdx, 1
    mov rax, 1
    mov rdi, 1
    syscall
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi,rdi
    jns print_uint    
    neg rdi       
    push rdi     
    mov rdi, '-' 
    call print_char
    pop rdi

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r11, rsp                
    mov rax, rdi                
    xor rdx, rdx                
    dec rsp                     
    mov byte[rsp],dl
    .loop: 
        mov rcx, 10                
        div rcx
        add rdx,'0'             
        dec rsp                 
        mov byte[rsp],dl
        xor rdx,rdx             
        cmp rax,0               
        ja .loop
    mov rdi,rsp                 
    push r11                    
    call print_string           
    pop rsp              
    ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx                    ; clearing counter
    .loop:                          ; start of comparing chars
        mov r10b, byte[rdi + rcx]   ; getting one char
        cmp r10b, byte[rsi + rcx]   ; comparing chars
        jne .false                  ; if not equal, return false
        test r10b, r10b             ; found null-terminator => return true
        jz .true
        inc rcx                     ; increment counter
        jmp .loop                   ; repeat
    
    .false:
        xor rax, rax      ; return false
        ret
    .true:
        mov rax, 1      ; return true
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0              ; preparing buffer to read, decrement rsp
    xor rax, rax        ; 0 - syscall code for 'read
    xor rdi, rdi        ; 0 - stdin descriptor
    mov rsi, rsp        ; giving address of the buffer
    mov rdx, 1          ; 1 - size of the buffer
    syscall
    pop rdi             ; getting char from the buffer
    mov al, dil   ; cutting to size of 1 char
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rcx,rcx                
    .loop:
        cmp rcx, rsi           
        jge .err              
        push rdi               
        push rsi
        push rcx
        call read_char         
        pop rcx                
        pop rsi
        pop rdi
        cmp al, 0x20          
        je .continue               
        cmp al, 0x9           
        je .continue
        cmp al, `\n`      
        je .continue
        mov byte[rdi+rcx],al   
        cmp al,0               
        je .end
        inc rcx                
        jmp .loop              

    .continue:
        test rcx,rcx                
        jz .loop               
    .end:                  
        mov rax, rdi           
        mov rdx, rcx           
        ret
    .err:
        xor rax, rax           
        ret


; 1st arg - buffer pointer (rdi)
; 2nd arg - buffer size (rsi)
; reads till '\n' (0xA)
; if success - returns buffer pointer (rax) and line length(rdx)
; adds null-terminator to a read line
; if fails - returns 0 (rax)
read_line:
    xor  rcx, rcx
    .loop:
        cmp  rcx, rsi
        je   .err
        push rdi
        push rsi
        push rcx
        call read_char
        pop  rcx
        pop  rsi
        pop  rdi
        cmp  al, `\n`
        je   .finish
        mov  byte[rdi+rcx], al
        cmp  al, 0
        je   .success
        inc  rcx
        jmp  .loop
    .err:
        xor  rax, rax
        ret
    .finish:
        mov  byte[rdi+rcx+1], 0
    .success:
        mov  rax, rdi
        mov  rdx, rcx
        ret



 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    .loop:
        cmp byte[rdi + rcx], '0'
        jl .end
        cmp byte[rdi + rcx], '9'
        jg .end
        mov rdx, 10
        mul rdx
        xor r10, r10
        mov r10b, byte[rdi + rcx]
        add rax, r10
        sub rax, '0'
        inc rcx
        jmp .loop
    .end:
        mov rdx, rcx
        ret
        




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    je .neg
    jmp parse_uint
    .neg:
        inc rdi
        call parse_uint
        inc rdx
        neg rax
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    .loop:
        cmp rax, rdx
        jge .err
        mov r10b, byte[rdi + rax]
        mov byte[rsi + rax], r10b
        cmp byte[rsi + rax], 0
        jz .end
        inc rax
        jmp .loop
    .err:
        xor rax, rax
    .end:
        ret
        
